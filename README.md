![DoStbin](doc/dostbin_logo.png)

DoStbin consists of two parts:
1. a data model (instantiating this [template](TEMPLATE.md)) for describing
   experiments involving
[Denial-of-Service](https://en.wikipedia.org/wiki/Denial-of-service_attack)
(DoS) attacks and mitigations, and
2. an [archive of results](data/) (organised using the data model) for DoS
   experiments done using [DoSarray](https://www.github.com/niksu/dosarray).
These experiments serve to better understand how to defend systems against
such attacks.

DoStbin emerged from research carried out at the University of Pennsylvania
as part of the [DeDOS project](http://dedos-project.net/).

## DoStbin data model
Experiment reports should include sufficient detail to enable others to
replicate the experiment and check if they also obtain the same results.
The data model helps standardise the level of detail on experiments.
It simple consists of a [template](TEMPLATE.md) that is filled out and
included it as a file called `README.md` with each of the experiments in our
archive, as in this [example](results/1/README.md).

## DoStbin archive of experiment results
__IMPORTANT__: DoSarray's raw and graph outputs are explained in the [DoSarray paper](https://www.seas.upenn.edu/~nsultana/dosarray/).

**No.** | **Target** | **Mitigation** | **Attack** | **Results**
--- | --- | --- | --- | ---
1 | Apache (Worker)| None | 5 x Slowloris | [Summary](results/1/graph_availability.pdf),  [View](results/1/README.md)
2 | Apache (Worker) | None | 5 x Slowloris | [Summary](results/2/graph_availability.pdf), [View](results/2/README.md)
3 | Apache (Worker) | None | 5 x Slowloris | [Summary](results/3/graph_availability.pdf), [View](results/3/README.md)
4 | Apache (Worker) | None | 5 x Tor's Hammer | [Summary](results/4/graph_availability.pdf), [View](results/4/README.md)
5 | Apache (Worker) | None | 5 x GoldenEye | [Summary](results/5/graph_availability.pdf), [View](results/5/README.md)
6 | Apache (Worker) | None | None | [Summary](results/6/graph_availability.pdf), [View](results/6/README.md)
7 | Nginx | None | 5 x Slowloris | [Summary](results/7/graph_availability.pdf), [View](results/7/README.md)
8 | Nginx | None | 5 x Tor's Hammer | [Summary](results/8/graph_availability.pdf), [View](results/8/README.md)
9 | Nginx | None | 5 x GoldenEye | [Summary](results/9/graph_availability.pdf), [View](results/9/README.md)
10 | Nginx | None | None | [Summary](results/10/graph_availability.pdf), [View](results/10/README.md)
11 | lighttpd | None | 5 x Slowloris | [Summary](results/11/graph_availability.pdf), [View](results/11/README.md)
12 | lighttpd | None | 5 x Tor's Hammer | [Summary](results/12/graph_availability.pdf), [View](results/12/README.md)
13 | lighttpd | None | 5 x GoldenEye | [Summary](results/13/graph_availability.pdf), [View](results/13/README.md)
14 | lighttpd | None | None | [Summary](results/14/graph_availability.pdf), [View](results/14/README.md)
15 | Apache (Event) | None | 5 x Slowloris | [Summary](results/15/graph_availability.pdf), [View](results/15/README.md)
16 | Apache (Event) | None | 5 x Tor's Hammer | [Summary](results/16/graph_availability.pdf), [View](results/16/README.md)
17 | Apache (Event) | None | 5 x GoldenEye | [Summary](results/17/graph_availability.pdf), [View](results/17/README.md)
18 | Apache (Event) | None | None | [Summary](results/18/graph_availability.pdf), [View](results/18/README.md)
19 | Varnish | None | 5 x Slowloris | [Summary](results/19/graph_availability.pdf), [View](results/19/README.md)
20 | Varnish | None | 5 x Tor's Hammer | [Summary](results/20/graph_availability.pdf), [View](results/20/README.md)
21 | Varnish | None | 5 x GoldenEye | [Summary](results/21/graph_availability.pdf), [View](results/21/README.md)
22 | Varnish | None | None | [Summary](results/22/graph_availability.pdf), [View](results/22/README.md)
23 | HA-Proxy | None | 5 x Slowloris | [Summary](results/23/graph_availability.pdf), [View](results/23/README.md)
24 | HA-Proxy | None | 5 x Tor's Hammer | [Summary](results/24/graph_availability.pdf), [View](results/24/README.md)
25 | HA-Proxy | None | 5 x GoldenEye | [Summary](results/25/graph_availability.pdf), [View](results/25/README.md)
26 | HA-Proxy | None | None | [Summary](results/26/graph_availability.pdf), [View](results/26/README.md)
27 | Apache (Worker) | iptables connlimit (300) | 5 x Slowloris | [Summary](results/27/graph_availability.pdf), [View](results/27/README.md)
28 | Apache (Worker) | iptables connlimit (300) | 5 x Tor's Hammer | [Summary](results/28/graph_availability.pdf), [View](results/28/README.md)
29 | Apache (Worker) | iptables connlimit (300) | 5 x GoldenEye | [Summary](results/29/graph_availability.pdf), [View](results/29/README.md)
30 | Apache (Worker) | iptables connlimit (300) | None | [Summary](results/30/graph_availability.pdf), [View](results/30/README.md)
31 | Apache (Worker) | iptables connlimit (10) | 5 x Slowloris | [Summary](results/31/graph_availability.pdf), [View](results/31/README.md)
32 | Apache (Worker) | iptables connlimit (10) | 5 x Tor's Hammer | [Summary](results/32/graph_availability.pdf), [View](results/32/README.md)
33 | Apache (Worker) | iptables connlimit (10) | 5 x GoldenEye | [Summary](results/33/graph_availability.pdf), [View](results/33/README.md)
34 | Apache (Worker) | iptables connlimit (10) | None | [Summary](results/34/graph_availability.pdf), [View](results/34/README.md)
35 | Apache (Worker) | iptables connlimit (3) | 5 x Slowloris | [Summary](results/35/graph_availability.pdf), [View](results/35/README.md)
36 | Apache (Worker) | iptables connlimit (3) | 5 x Tor's Hammer | [Summary](results/36/graph_availability.pdf), [View](results/36/README.md)
37 | Apache (Worker) | iptables connlimit (3) | 5 x GoldenEye | [Summary](results/37/graph_availability.pdf), [View](results/37/README.md)
38 | Apache (Worker) | iptables connlimit (3) | None | [Summary](results/38/graph_availability.pdf), [View](results/38/README.md)
39 | Apache (Event) | iptables connlimit (10) | 5 x Slowloris | [Summary](results/39/graph_availability.pdf), [View](results/39/README.md)
40 | Apache (Event) | iptables connlimit (10) | 5 x Tor's Hammer | [Summary](results/40/graph_availability.pdf), [View](results/40/README.md)
41 | Apache (Event) | iptables connlimit (10) | 5 x GoldenEye | [Summary](results/41/graph_availability.pdf), [View](results/41/README.md)
42 | Apache (Event) | iptables connlimit (10) | None | [Summary](results/42/graph_availability.pdf), [View](results/42/README.md)
43 | Apache (Event) | iptables connlimit (3) | 5 x Slowloris | [Summary](results/43/graph_availability.pdf), [View](results/43/README.md)
44 | Apache (Event) | iptables connlimit (3) | 5 x Tor's Hammer | [Summary](results/44/graph_availability.pdf), [View](results/44/README.md)
45 | Apache (Event) | iptables connlimit (3) | 5 x GoldenEye | [Summary](results/45/graph_availability.pdf), [View](results/45/README.md)
46 | Apache (Event) | iptables connlimit (3) | None | [Summary](results/46/graph_availability.pdf), [View](results/46/README.md)
47 | Apache (Event) | iptables connlimit (3) | 2 x Slowloris | [Summary](results/47/graph_availability.pdf), [View](results/47/README.md)
48 | Apache (Event) | iptables connlimit (3) | 2 x Tor's Hammer | [Summary](results/48/graph_availability.pdf), [View](results/48/README.md)
49 | Apache (Event) | iptables connlimit (3) | 2 x GoldenEye | [Summary](results/49/graph_availability.pdf), [View](results/49/README.md)
50 | Apache (Event) | iptables connlimit (3) | None | [Summary](results/50/graph_availability.pdf), [View](results/50/README.md)
51 | Apache (Event) | None | 2 x Slowloris | [Summary](results/51/graph_availability.pdf), [View](results/51/README.md)
52 | Apache (Event) | None | 2 x Tor's Hammer | [Summary](results/52/graph_availability.pdf), [View](results/52/README.md)
53 | Apache (Event) | None | 2 x GoldenEye | [Summary](results/53/graph_availability.pdf), [View](results/53/README.md)
54 | Apache (Event) | None | None | [Summary](results/54/graph_availability.pdf), [View](results/54/README.md)

__WIP__: Other experiments have been made but the results haven't been published yet.

## Contributing to the archive
Simply create a Merge Request with your experiment results formatted as in the
above entries.

Both new experiments as well as re-doing of experiments already in the archive are welcome. Feel free to replicate an experiment that is
already included in the archive (or a variation of an experiment) and send in
your results.

Together with your experiment results, please provide important metadata for
your experiment by filling in this [template](TEMPLATE.md), and include it as
a file called `README.md`, as in this [example](results/1/README.md).

## Contributors
Zihao Jin (Tsinghua University),
Pardis Pashakhanloo (University of Pennsylvania),
Nik Sultana (University of Pennsylvania).

## Current version
DoStbin v1

## License
See [LICENSE](LICENSE) file.

## Maintainer
[Nik Sultana](https://www.seas.upenn.edu/~nsultana)
