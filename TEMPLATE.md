```
Text marked up in this manner provides a description of each field,
and guidance for filling it in. Please remove this text such when
you create your "README.md" by filling in this template.
```

## Description
* **Target**: `version+configuration+parameters of the software being targetted in this experiment`
* **Mitigation**: `if a mitigation is involved in the experiment, describe the version+configuration+parameters for what is used to provide the mitigation`
* **Attack**: `the version+configuration+parameters and possibly a location (e.g., URL) of code that is used to perform the attack.`

## Results
* **Availability**: `links or details of the experiment outcomes`
* **Load**: `links or details of load measurements done during the experiment`

## Dimensions
* **Number of runs**: `Number of times the experiment was rerun. The results of each run are made available in separate subdirectories.`
* **Number of attacks**: `Number of attacks that occur during the experiment.`
* **Max number of simultaneous attacks**: `Number of simultaneous attacks that occur during the experiment.`
* **Total experiment duration**: `Total duration of the experiment in seconds`
* **Interval between load measurements**: `Number of seconds that elapses between load measurements made on each physical node`
* **Virtual network size**: `(Number of virtual nodes)` nodes
* **Physical network size**: `(Number of physical nodes)` nodes
* **Description of the experiment**: `Brief outline of the experiment's timeline, such as when the attack(s) start and their duration, and whether there are different types of attacks taking place.`

## Contributor
`Your name + email, if you wish.`

## Date
`When the experiment took place.`

## DoSarray version
0.2 `Which version of DoSarray was used to carry out the experiment.`

## DoSarray container image
dosarray_image_v0.2 `Which container image + version was used in the experiment.`

## Inclusions
- [x] raw logs `Tick this if you include raw logs in your contribution. Can also make this into a link to the directory where the logs are stored.`
- [x] processed logs `Tick this if you include data derived from the raw logs in your contribution. Can also make this into a link to the directory where the logs are stored.`
- [x] rendered graphs `Tick this if you include graphs in your contribution. Can also make this into a link to the directory where the graphs are stored.`
- [ ] code `Tick this if you include code in your contribution, to automate parts of the experiment for example. Can also make this into a link to the directory where the code is stored.`

```
__IMPORTANT__
* If you include code you wrote, then be sure to clearly specify which license you choose to distributing that code under, inside each code file.
* If you include code you didn't write, then make sure that you comply fully with that code's license. If in doubt, leave it out.
```

## Experiment scripting
`If the code that generated the data isn't available, its behaviour could be
summarised here.`

## Experiment environment
* **OS versions**: `OSs used on the physical nodes`
* **Docker versions**: `Docker versions used on the physical nodes`
* **Physical network**: `Info about the physical network over which the experiment took place.`
* **Physical machines**: `Info about the physical machines -- such as CPU models and RAM quantity.`

## Related experiments
`Numbered list of links to related experiments in this archive or elsewhere, and a brief note of how they're related.`

## Other
`Other information about the experiment that didn't fit in the previous sections, including acknowledgements.`

## DoStbin
v1 `Do not change this value.`
