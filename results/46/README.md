## Description
* **Target**: Apache/2.4.26 (Unix) using default configuration, and the Event MPM.
* **Mitigation**: Limiting to 3 connections for each source host address, by using `sudo iptables -A INPUT -p tcp --syn --dport 8013 -m connlimit --connlimit-above 3 -j REJECT --reject-with tcp-reset` (where `8013` is the port that Apache is listening on)
* **Attack**: None

## Results
* **Availability**: [Summary](results/46/graph_availability.pdf), [Detail](results/46/graph.pdf), [Contour](results/46/graph_contour.pdf)
* **Load**: [CPU](results/46/load.pdf), [Memory](results/46/mem.pdf), [Net Rx](results/46/net_rx.pdf), [Net Tx](results/46/net_tx.pdf), [Net Rx Errors](results/46/net_rxerrors.pdf), [Net Tx Errors](results/46/net_txerrors.pdf)

## Dimensions
* **Number of runs**: 1
* **Number of attacks**: 0
* **Max number of simultaneous attacks**: 0
* **Total experiment duration**: 65 seconds
* **Interval between load measurements**: 5 seconds
* **Virtual network size**: 70 nodes (10 virtual on each of 7 physical nodes)
* **Physical network size**: 8 nodes
* **Description of the experiment**:
  * 0 seconds: experiment starts
  * 65 seconds: experiment ends

## Contributor
[Nik Sultana](https://www.seas.upenn.edu/~nsultana)

## Date
Wed Jun 20 17:48:40 EDT 2018

## DoSarray version
0.2

## DoSarray container image
dosarray_image_v0.2

## Inclusions
- [x] [raw logs](results/46/)
- [x] [processed logs](results/46/)
- [x] [rendered graphs](results/46/)
- [ ] code

## Experiment scripting
The [script for this experiment](https://github.com/niksu/dosarray/blob/b31b0c8fd1882308aed8c5e1eab2743b204b73f3/experiments/dosarray_multi_experiment.sh) is included with DoSarray as an example of how to run a set of experiments.

## Experiment environment
* **OS versions**: All nodes run Ubuntu 14.04.5 LTS: Linux dedos01 4.4.0-31-generic #50~14.04.1-Ubuntu SMP Wed Jul 13 01:07:32 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
* **Docker versions**: All nodes run version 17.09.1-ce, build 19e2cf6
* **Physical network**: 10GbE data network and 1GbE control network.
* **Physical machines**: Intel(R) Xeon(R) CPU E5-2630L v3 @ 1.80GHz, 64GB RAM, Dell 072T6D version A01

## Related experiments
This is similar to experiment [42](../42/README.md) but uses a different mitigation parameter.

## Other
This experiment was carried out at the University of Pennsylvania
as part of the [DeDOS project](http://dedos-project.net/),
supported by the Defense Advanced Research Projects Agency (DARPA) under
Contract No. HR0011-16-C-0056. Any opinions, findings and conclusions or
recommendations expressed in this material are those of the authors and do
not necessarily reflect the views of DARPA.

## DoStbin
v1
