## Description
* **Target**: Apache/2.4.26 (Unix) using default configuration, and the Worker MPM.
* **Mitigation**: None
* **Attack**: slowloris.pl obtained online (MD5 3388f551d88a12fa6cbb4aa7b2c499f8), run without any tuning (other than specifying `-dns` and `-port` parameters).

## Results
### Run 1
* **Availability**: [Summary](results/2/1/graph_availability.pdf), [Detail](results/2/1/graph.pdf), [Contour](results/2/1/graph_contour.pdf)
* **Load**: [CPU](results/2/1/load.pdf), [Memory](results/2/1/mem.pdf), [Net Rx](results/2/1/net_rx.pdf), [Net Tx](results/2/1/net_tx.pdf), [Net Rx Errors](results/2/1/net_rxerrors.pdf), [Net Tx Errors](results/2/1/net_txerrors.pdf)

### Run 2
* **Availability**: [Summary](results/2/2/graph_availability.pdf), [Detail](results/2/2/graph.pdf), [Contour](results/2/2/graph_contour.pdf)
* **Load**: [CPU](results/2/2/load.pdf), [Memory](results/2/2/mem.pdf), [Net Rx](results/2/2/net_rx.pdf), [Net Tx](results/2/2/net_tx.pdf), [Net Rx Errors](results/2/2/net_rxerrors.pdf), [Net Tx Errors](results/2/2/net_txerrors.pdf)

### Run 3
* **Availability**: [Summary](results/2/3/graph_availability.pdf), [Detail](results/2/3/graph.pdf), [Contour](results/2/3/graph_contour.pdf)
* **Load**: [CPU](results/2/3/load.pdf), [Memory](results/2/3/mem.pdf), [Net Rx](results/2/3/net_rx.pdf), [Net Tx](results/2/3/net_tx.pdf), [Net Rx Errors](results/2/3/net_rxerrors.pdf), [Net Tx Errors](results/2/3/net_txerrors.pdf)

### Run 4
* **Availability**: [Summary](results/2/4/graph_availability.pdf), [Detail](results/2/4/graph.pdf), [Contour](results/2/4/graph_contour.pdf)
* **Load**: [CPU](results/2/4/load.pdf), [Memory](results/2/4/mem.pdf), [Net Rx](results/2/4/net_rx.pdf), [Net Tx](results/2/4/net_tx.pdf), [Net Rx Errors](results/2/4/net_rxerrors.pdf), [Net Tx Errors](results/2/4/net_txerrors.pdf)

## Dimensions
* **Number of runs**: 4
* **Number of attacks**: 5
* **Max number of simultaneous attacks**: 5
* **Total experiment duration**: 65 seconds
* **Interval between load measurements**: 5 seconds
* **Virtual network size**: 70 nodes (10 virtual on each of 7 physical nodes)
* **Physical network size**: 8 nodes
* **Description of the experiment**:
  * 0 seconds: experiment starts
  * 10 seconds: attack starts
  * 30 seconds: attack ends
  * 65 seconds: experiment ends

## Contributor
[Nik Sultana](https://www.seas.upenn.edu/~nsultana)

## Date
Wed Jun  6 23:56:35 EDT 2018

## DoSarray version
0.2

## DoSarray container image
dosarray_image_v0.2

## Inclusions
- [x] [raw logs](results/2/)
- [x] [processed logs](results/2/)
- [x] [rendered graphs](results/2/)
- [ ] code

## Experiment scripting
The script for this experiment is included with DoSarray as an [example experiment](https://github.com/niksu/dosarray/blob/b31b0c8fd1882308aed8c5e1eab2743b204b73f3/experiments/dosarray_slowloris_experiment.sh).

## Experiment environment
* **OS versions**: All nodes run Ubuntu 14.04.5 LTS: Linux dedos01 4.4.0-31-generic #50~14.04.1-Ubuntu SMP Wed Jul 13 01:07:32 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
* **Docker versions**: All nodes run version 17.09.1-ce, build 19e2cf6
* **Physical network**: 10GbE data network and 1GbE control network.
* **Physical machines**: Intel(R) Xeon(R) CPU E5-2630L v3 @ 1.80GHz, 64GB RAM, Dell 072T6D version A01

## Related experiments
This is a rerun of experiment [1](../1/README.md).

## Other
This experiment was carried out at the University of Pennsylvania
as part of the [DeDOS project](http://dedos-project.net/),
supported by the Defense Advanced Research Projects Agency (DARPA) under
Contract No. HR0011-16-C-0056. Any opinions, findings and conclusions or
recommendations expressed in this material are those of the authors and do
not necessarily reflect the views of DARPA.

## DoStbin
v1
